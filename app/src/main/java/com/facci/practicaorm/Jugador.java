package com.facci.practicaorm;

import com.orm.SugarRecord;

public class Jugador extends SugarRecord {

    String idjugador, nombre;

    public Jugador() {
    }

    public Jugador(String idjugador, String nombre) {
        this.idjugador = idjugador;
        this.nombre = nombre;
    }

    public String getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(String idjugador) {
        this.idjugador = idjugador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
