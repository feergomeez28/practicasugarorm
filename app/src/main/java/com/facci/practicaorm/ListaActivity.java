package com.facci.practicaorm;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListaActivity extends AppCompatActivity {

    ListView listViewJugadores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        listViewJugadores = (ListView) findViewById(R.id.listViewJugadores);
        cargarListView();
    }

    private void cargarListView() {
        List<Jugador> resultadoConsulta = Jugador.listAll(Jugador.class);
        ArrayList<String> lista = new ArrayList<String>();
        for(int i=0; i<resultadoConsulta.size();i++){
            Jugador resultado = resultadoConsulta.get(i);
            lista.add(resultado.getIdjugador()+" - "+resultado.getNombre());
        }
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, lista);
        listViewJugadores.setAdapter(adaptador);
    }


}
