package com.facci.practicaorm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

import static com.orm.SugarRecord.find;

public class MainActivity extends AppCompatActivity {

    TextView lblId, lblNombreJugador;
    EditText txtId, txtNombreJugador;
    Button btnAgregar, btnActualizar, btnBuscar, btnLista, btnEliminar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lblId = (TextView) findViewById(R.id.lblID);
        lblNombreJugador = (TextView) findViewById(R.id.lblNombreJugador);
        txtId = (EditText) findViewById(R.id.txtID);
        txtNombreJugador = (EditText) findViewById(R.id.txtNombreJugador);
        btnAgregar = (Button) findViewById(R.id.btnAgregar);
        btnActualizar = (Button) findViewById(R.id.btnActualizar);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);
        btnEliminar = (Button) findViewById(R.id.btnEliminar);
        btnLista = (Button) findViewById(R.id.btnLista);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Jugador jugador = new Jugador(txtId.getText().toString(), txtNombreJugador.getText().toString());
                jugador.save();

                //Mensaje
                Toast.makeText(MainActivity.this, "Registro guardado exitosamente", Toast.LENGTH_SHORT).show();
                //Limpiar los campos de texto
                txtId.setText("");
                txtNombreJugador.setText("");
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Jugador actualizar = Jugador.findById(Jugador.class, Integer.parseInt(txtId.getText().toString()));
                actualizar.nombre = txtNombreJugador.getText().toString();
                actualizar.save();

                //Mensaje
                Toast.makeText(MainActivity.this, "Registro actualizado exitosamente", Toast.LENGTH_SHORT).show();
                //Limpiar los campos de texto
                txtId.setText("");
                txtNombreJugador.setText("");
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Jugador> resultado = Jugador.find(Jugador.class, "idjugador=?", txtId.getText().toString());
                Jugador datos = resultado.get(0);
                lblId.setText("ID: " + datos.getIdjugador());
                lblNombreJugador.setText("Nombre: " + datos.getNombre());

                //Limpiar los campos de texto
                txtId.setText("");
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Jugador eliminarJugador = Jugador.findById(Jugador.class, Integer.parseInt(txtId.getText().toString()));
                eliminarJugador.delete();

                //Mensaje
                Toast.makeText(MainActivity.this, "Registro eliminado exitosamente", Toast.LENGTH_SHORT).show();
                //Limpiar los campos de texto
                txtId.setText("");
            }
        });

        btnLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListaActivity.class);
                startActivity(intent);
            }
        });
    }
}